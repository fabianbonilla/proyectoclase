package com.bonilla.proyecto1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class InfoEquipo extends AppCompatActivity {

    WebView paginaEquipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_equipo);

        paginaEquipo=findViewById(R.id.paginaEquipo);

        //Capturar el objeto Intent
        Intent myIntent=getIntent();

        String dato=myIntent.getStringExtra("nombre");

        dato.replaceAll(" ", "_");

        String url="https://es.wikipedia.org/wiki/"+dato;

        //Deshabilitar el navegador predeterminado
        paginaEquipo.setWebViewClient(new WebViewClient());

        paginaEquipo.loadUrl(url);

    }
}