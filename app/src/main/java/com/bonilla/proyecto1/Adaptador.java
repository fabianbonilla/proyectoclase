package com.bonilla.proyecto1;

import android.hardware.lights.LightState;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adaptador extends RecyclerView.Adapter<Elemento> {
    List<String> elementos;

    // crear un constructor
    public Adaptador(List<String> datos){

        //inicializa los datos
        elementos=datos;
    }


    @NonNull
    @Override
    public Elemento onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //se debe utilizar un creador e inflador de layout para ingresar datos (Layoutinflater)

        LayoutInflater miCreadordeCajones;

        //de donde se van a tomar los datos


        View miCajon;

        //el comtexto es el de reciclerview parent.getContext()
        //para traer e instanciar (inflar) del xml el layout se debe utilizar un View por el metodo Inflater para verlo en tipo vista
        miCajon= LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento,parent,false);

        return new Elemento(miCajon);

    }

    @Override
    public void onBindViewHolder(@NonNull Elemento holder, int position) {

        String miDato= elementos.get(position);
        holder.tvElemento.setText(miDato);

    }

    // cuantos datos hay en el reciclerview
    @Override
    public int getItemCount() {

        if (elementos!= null){
            return elementos.size();

        }else
            return 0;

        //coja la posición y guardela




    }
}