package com.bonilla.proyecto1;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

public class MapaAndroid extends AppCompatActivity {

    MapView mapa;
    IMapController mapaControlador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_android);
        mapa= (MapView) findViewById(R.id.mapa);

        //obtener el contexto
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.setMultiTouchControls(true);

        String[] permisos = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(permisos,1);
        }

        GeoPoint miPunto = new GeoPoint(4.62,-74.18);
        Marker miMarcador = new Marker(mapa);
        miMarcador.setPosition(miPunto);
        mapa.getOverlays().add(miMarcador);

        mapaControlador= mapa.getController();
        mapaControlador.setCenter(miPunto);
        mapaControlador.setZoom(15.0);

    }

}