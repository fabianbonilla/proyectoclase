package com.bonilla.proyecto1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class UltraCalculadora extends AppCompatActivity implements View.OnClickListener {


    //1. crear los objetos que se relacionaran con los botones de layout
    MaterialButton boton1, boton2, boton3, boton4, boton5, boton6, boton7,boton8,boton9,boton0, botonPI, botonPD, btn_suma, btn_resta, btn_division, btn_multiplicacion, boton_C, boton_Ac;
    TextView expresion, resultado;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ultra_calculadora);


        expresion = findViewById(R.id.expresion);
        resultado = findViewById(R.id.resultado);
        //2. realicionar el objeto boton1 con el elemento cuyo id esa boton1
        boton1= findViewById(R.id.boton_1);
        boton2=findViewById(R.id.boton_2);
        boton3=findViewById(R.id.boton_3);
        boton4=findViewById(R.id.boton_4);
        boton5=findViewById(R.id.boton_5);
        boton6=findViewById(R.id.boton_6);
        boton7=findViewById(R.id.boton_7);
        boton8=findViewById(R.id.boton_8);
        boton9=findViewById(R.id.boton_9);
        boton0=findViewById(R.id.boton_0);
        botonPD = findViewById(R.id.boton_parentesis_d);
        botonPI = findViewById(R.id.boton_parentesis_i);
        btn_suma = findViewById(R.id.boton_sum);
        boton_C = findViewById(R.id.boton_C);
        boton_Ac = findViewById(R.id.boton_ac);



        //3. convertir el boton en un listener
        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);
        boton7.setOnClickListener(this);
        boton8.setOnClickListener(this);
        boton9.setOnClickListener(this);
        boton0.setOnClickListener(this);
        botonPI.setOnClickListener(this);
        botonPD.setOnClickListener(this);
        btn_suma.setOnClickListener(this);
        boton_C.setOnClickListener(this);
        boton_Ac.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        // 4.crear un objeto que represente el boton al cual se la ha hecho click
        MaterialButton boton = (MaterialButton) view;
        String texto;
        texto = boton.getText().toString();


        this.expresion.setText(this.expresion.getText().toString()+texto);

        boton_C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expresion.setText("");
            }
        });

        boton_Ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expresion.setText("");
                resultado.setText("");
            }
        });



    }

}