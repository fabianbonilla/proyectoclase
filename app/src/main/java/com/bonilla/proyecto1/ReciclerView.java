package com.bonilla.proyecto1;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReciclerView extends AppCompatActivity {

    RecyclerView rvElementos;

    Button miBoton1, miBoton2, miBoton3;

    //cfrear el objeto

    Adaptador miAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recicler_view);

        miBoton1 = findViewById(R.id.buttonCalculadora);
        miBoton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( ReciclerView.this, UltraCalculadora.class);
                startActivity(intent);
            }
        });

        miBoton2 = findViewById(R.id.buttonMapa);
        miBoton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( ReciclerView.this, MapaAndroid.class);
                startActivity(intent);
            }
        });

        miBoton3 = findViewById(R.id.buttonClub);
        miBoton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( ReciclerView.this, futbolprofesional.class);
                startActivity(intent);
            }
        });



        //tener el recyclerview
        rvElementos=findViewById(R.id.rvElementos);

        rvElementos.setLayoutManager(new LinearLayoutManager(this));

        //definir datos pueden obtenerse de diferentes sitios

       /* getElementos();*/

        //1. datos locales definidos dentro de la app aquí:

        List<String> misDatosLocales= new ArrayList<>();
        misDatosLocales.add("Fabian Alberto");
        misDatosLocales.add("Bonilla Ascencio");
        misDatosLocales.add("Policia Nacional");



        //tomar los datos desde un array

       // List<String> misDatosString= Arrays.asList(getResources().getStringArray(R.array.nombres));



        // crear un robot de la clase adpatador para que tome los datos y los meta en una cajita y los ubique en un recycler view

        miAdaptador= new Adaptador(misDatosLocales);
        rvElementos.setAdapter(miAdaptador);

       /* getElementos();*/
    }
 /*  public void getElementos (){

        String miUrl="https://www.balldontlie.io/api/v1/teams";
        RequestQueue miPila= Volley.newRequestQueue(this);


        JsonObjectRequest miPeticion;

        //crear el listner es la clase especifica que nos permite trabajar con los eventos asincronos que existan en un evento

        miPeticion = new JsonObjectRequest(Request.Method.GET, miUrl, null,


                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //conectarnos obtener los datos que nos devolvio o retornó la API

                        try {

                            // rescatar o guardar arreglos de datos
                            JSONArray miArreglo = response.getJSONArray("data");
                            // con el arreglo luego se puede generar las lista de elementos nombres
                            List<String> elementos = new ArrayList<>();

                            //recorrer el arreglo

                            for (int i = 0; i < miArreglo.length(); i++) {

                                JSONObject miElemento = miArreglo.getJSONObject(i);

                                elementos.add(miElemento.getString("full_name"));

                            }

                            // Mostrar los resultados en el RecyclerView

                            RecyclerView rv;
                            rv = findViewById(R.id.rvElementos);

                            // crear el adaptador

                            miAdaptador = new Adaptador(elementos);

                            rv.setAdapter((miAdaptador));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "Hubo un error sr yefer");
            }
        }


        );

        miPila.add(miPeticion);
    }*/
}